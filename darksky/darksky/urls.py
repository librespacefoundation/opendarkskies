"""Base Django URL mapping for DarkSky"""

from django.contrib import admin
from django.urls import include, path
from light_measurements.urls import API_URLPATTERNS

urlpatterns = [path("admin/", admin.site.urls), path('api/', include(API_URLPATTERNS))]
