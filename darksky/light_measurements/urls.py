"""DarkSky API URL mappings"""
from light_measurements import views
from rest_framework import routers

ROUTER = routers.DefaultRouter()

ROUTER.register(r"light_measurements", views.LightMeasurementView, basename="measurement")

API_URLPATTERNS = ROUTER.urls
