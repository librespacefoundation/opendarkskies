import django_filters
from django_filters import rest_framework as filters

from .models import LightMeasurement


class LightMeasurementFilter(filters.FilterSet):
    """Filterset for LightMeasurements"""
    min_date = django_filters.DateTimeFilter(field_name="timestamp", lookup_expr='gte')
    max_date = django_filters.DateTimeFilter(field_name="timestamp", lookup_expr='lte')

    class Meta:
        model = LightMeasurement
        fields = ['min_date', 'max_date']
