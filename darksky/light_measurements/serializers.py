from datetime import datetime

from django.utils import timezone
from rest_framework import serializers

from .models import LightMeasurement


class ThreeValuesField(serializers.ListField):
    """Field for a list of three float values"""

    child = serializers.FloatField(allow_null=True)


class UnixTimestampField(serializers.Field):
    """Field to get UNIX timestamps converted into python datetime"""

    def to_representation(self, value):
        return int(value.timestamp())

    def to_internal_value(self, data):
        return timezone.make_aware(datetime.utcfromtimestamp(data))


class LightMeasurementSerializer(serializers.ModelSerializer):
    """LightMeasurement API Serializer"""

    ts = UnixTimestampField(source="timestamp")
    bat = serializers.FloatField(allow_null=True, required=False, source="battery")
    accel = ThreeValuesField(allow_null=True, required=False, min_length=3, max_length=3)
    lux = ThreeValuesField(allow_null=True, required=False, min_length=3, max_length=3)
    amb = ThreeValuesField(allow_null=True, required=False, min_length=3, max_length=3)
    accel_sd = ThreeValuesField(allow_null=True, required=False, min_length=3, max_length=3)
    lux_sd = ThreeValuesField(allow_null=True, required=False, min_length=3, max_length=3)
    amb_sd = ThreeValuesField(allow_null=True, required=False, min_length=3, max_length=3)

    class Meta:
        model = LightMeasurement
        fields = [
            "id",
            "sensor_id",
            "ts",
            "bat",
            "accel",
            "accel_sd",
            "lux",
            "lux_sd",
            "amb",
            "amb_sd",
            "lat",
            "lon",
            "alt",
        ]

    def to_internal_value(self, data):
        """Separates the amb(ambient) list into the respective values"""
        validated_data = super().to_internal_value(data)

        amb_value = validated_data.pop("amb")
        (
            validated_data["temp"],
            validated_data["humidity"],
            validated_data["pressure"],
        ) = amb_value or [None, None, None]

        amb_sd_value = validated_data.pop("amb_sd")
        (
            validated_data["temp_sd"],
            validated_data["humidity_sd"],
            validated_data["pressure_sd"],
        ) = amb_sd_value or [None, None, None]

        return validated_data

    def to_representation(self, instance):
        """Merges temp, humidity and pressure into a list 'amb' (ambient)"""
        instance.amb = None
        instance.amb_sd = None
        representation = super().to_representation(instance)

        representation["amb"] = None if not any(
            (instance.temp, instance.humidity, instance.pressure)
        ) else [
            instance.temp,
            instance.humidity,
            instance.pressure,
        ]

        representation["amb_sd"] = None if not any(
            (instance.temp, instance.humidity, instance.pressure)
        ) else [
            instance.temp_sd,
            instance.humidity_sd,
            instance.pressure_sd,
        ]

        return representation
