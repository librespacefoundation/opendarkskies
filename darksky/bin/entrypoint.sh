#!/bin/sh -e
#
# SatNOGS Django control script
#
# Copyright (C) 2018-2019 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

MANAGE_CMD="django-admin"
WSGI_SERVER="gunicorn"
DJANGO_APP="darksky"
FRONTEND_DIR="frontend-dist"

usage() {
	cat <<EOF
Usage: $(basename "$0") [OPTIONS]... [COMMAND]...
SatNOGS Django control script.

COMMANDS:
  prepare               Collect static files, compress templates and
                         apply migrations
  initialize            Load initial fixtures
  run_api                   Run WSGI HTTP server
  develop_api [SOURCE_DIR]  Run application in development mode, optionally
                         installing SOURCE_DIR in editable mode
  run_ingestion			Run MQTT Client  

OPTIONS:
  --help                Print usage
EOF
	exit 1
}

prepare() {
	"$MANAGE_CMD" collectstatic --noinput --clear
	"$MANAGE_CMD" migrate --noinput
}

move_frontend_dist_into_volume() {
	# Move the dist(build artifact of darksky-frontend that was created in darksky/Dockerfile.prod)
	# into the volume that is shared with nginx 
	rm -rf ../darksky-frontend/*
	cp -r ../dist/* ../darksky-frontend
}

run_api() {
	move_frontend_dist_into_volume
	prepare
	exec "$WSGI_SERVER" --bind 0.0.0.0:8000 "$DJANGO_APP".wsgi
}

develop_api() {
	if [ -d "$1" ]; then
		install_editable "$1"
	fi

	prepare
	exec "$MANAGE_CMD" runserver 0.0.0.0:8000
}

run_ingestion() {
	prepare
	exec "$MANAGE_CMD" run_mqtt_client
}

initialize() {
	"$MANAGE_CMD" initialize
}

parse_args() {
	while [ $# -gt 0 ]; do
		arg="$1"
		case $arg in
			prepare|run_api|initialize|run_ingestion)
				command="$arg"
				break
				;;
			develop_api)
				shift
				subargs="$1"
				command="$arg"
				break
				;;
			*)
				usage
				exit 1
				;;
		esac
		shift
	done
}

main() {
	parse_args "$@"
	if [ -z "$command" ]; then
		usage
		exit 1
	fi
	if [ -x "manage.py" ]; then
		MANAGE_CMD="./manage.py"
	fi
	"$command" "$subargs"
}

main "$@"
