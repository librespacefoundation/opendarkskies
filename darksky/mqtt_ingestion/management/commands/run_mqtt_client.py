import io

import paho.mqtt.client as mqtt
from django.conf import settings
from django.core.management.base import BaseCommand
from light_measurements.serializers import LightMeasurementSerializer
from rest_framework.exceptions import ParseError
from rest_framework.parsers import JSONParser


def save_instance_from_payload(payload, topic):
    """Validates that the received MQTT message payload represents a LightMeasurement"""
    stream = io.BytesIO(payload)
    try:
        data = JSONParser().parse(stream)
        sensor_id = topic.split("/")[-1]
        print(f"Received payload from sensor with ID :{sensor_id}")
    except ParseError as err:
        data = None
        error_msg = "The payload could not be parsed as JSON."
        raise ValueError(error_msg) from err
    data["sensor_id"] = sensor_id
    measurement = LightMeasurementSerializer(data=data)
    if not measurement.is_valid():
        error_msg = "The measurement data are invalid."
        raise ValueError(error_msg)
    measurement.save()


class Command(BaseCommand):
    """Starts the MQTT client and saves received Light Measurements to database"""

    help = "Description of your command."

    def on_connect(self, client, userdata, flags, result_code):  # pylint: disable=W0613
        """Subscribes to the measurement data sources"""
        print("Connected with result code " + str(result_code))
        for topic in settings.MQTT_TOPICS:
            client.subscribe(topic)

    def on_message(self, client, userdata, msg):  # pylint: disable=W0613
        """Saves the received measurements"""
        try:
            save_instance_from_payload(msg.payload, msg.topic)
        except ValueError as err:
            print(str(err))

    def handle(self, *args, **options):
        print("Starting MQTT client...")
        mqtt_client = mqtt.Client()
        mqtt_client.on_connect = self.on_connect
        mqtt_client.on_message = self.on_message
        mqtt_client.connect(settings.MQTT_HOST, settings.MQTT_PORT, settings.MQTT_KEEPALIVE)
        mqtt_client.loop_forever()
