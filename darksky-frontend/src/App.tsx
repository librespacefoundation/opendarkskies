import Container from 'react-bootstrap/Container';
import Map from './modules/Map/Map';

function App() {
  return (
    <Container>
      <h1>Darksky</h1>
      <Map />
    </Container>
  );
}

export default App;
