export interface LightMeasurement {
  id: number;
  ts: string;
  bat: number;
  accel: Array<number>;
  accel_sd: Array<number>;
  lux: Array<number>;
  lux_sd: Array<number>;
  amb: Array<number>;
  amb_sd: Array<number>;
  lat: number;
  lon: number;
  alt: number;
}
