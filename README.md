# Open Dark Skies
A crowdsourced open database of dark sky quality measurements.

> On the following docker compose commands, replace `docker-compose` with `docker compose` depending on which version you are using

## Setting up for development
On the command line, build the containers:
```sh
docker-compose up -d --build
```

Then set up the frontend:
```sh
cd darksky-frontent
npm install
npm run dev
```

## Setting up the test transmitter
There is a container that emulates a transmitter that transmits data to the MQTT broker. You can enable it by running:
```sh
docker-compose up -f docker-compose.yml -f docker-compose.test.yml up -d --build
```

## Setting up production
For the production setup, the frontent is built as a first stage in a two-stage docker build in the nginx container. Then, the static files produced will be served by the nginx container.

```sh
docker-compose up -f docker-compose.yml -f docker-compose.prod.yml up -d --build
```

## License

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2018-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)


