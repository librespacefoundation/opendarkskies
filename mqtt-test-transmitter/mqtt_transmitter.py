import json
import random
import time

import paho.mqtt.client as mqtt

print("Starting MQTT client...")


def on_connect(mqtt_client, userdata, flags, result_code):  # pylint: disable=W0613
    """Callback function when the client connects to the broker"""
    print(f"Connected with result code {result_code}")


def create_test_data_frame():
    """Generates a data frame for testing"""
    dataframe = {
        "ts": int(time.time()),  # Unix timestamp
        "bat": 4.167187,  # Battery voltage
        "accel": [-0.0012748645, -0.2587975, -10.163221],  # x axis  # y axis  # z axis
        "lux": [23981, 46372, -10.294291],  # Channel 1  # Channel 2  # Calibrated value
        "amb": [
            25.810051,  # Temperature
            68.154015,  # Relative humidity
            100974.91,  # Pressure
        ],
        "accel_sd": [
            0.00698272,  # Acceleration (Standard deviation)
            0.025965577,
            0.026446905,
        ],
        "lux_sd": [18900.457, 18424.037, 1.6056948],  # Light (Standard deviation)
        "amb_sd": [
            0.59440166,  # Ambient parameters (Standard deviation)
            0.8472905,
            5311.8823,
        ],
        "lat": random.uniform(-90, 90),  # Latitude
        "lon": random.uniform(-180, 180),  # Longitude
        "alt": 120,  # Altitude
    }

    return dataframe


def create_partial_test_data_frame():
    """Generates a data frame for testing"""
    dataframe = {
        "ts": int(time.time()),  # Unix timestamp
        "bat": None,  # Battery voltage
        "accel": [None, -0.2587975, -10.163221],  # x axis  # y axis  # z axis
        "lux": [None, 46372, -10.294291],  # Channel 1  # Channel 2  # Calibrated value
        "amb": [
            None,
            68.154015,  # Relative humidity
            100974.91,  # Pressure
        ],
        "accel_sd": None,
        "amb_sd": [
            0.59440166,  # Ambient parameters (Standard deviation)
            0.8472905,
            5311.8823,
        ],
        "lat": random.uniform(-90, 90),  # Latitude
        "lon": random.uniform(-180, 180),  # Longitude
    }

    return dataframe


# Create an MQTT client instance
client = mqtt.Client()

# Assign the callback functions to the client instance
client.on_connect = on_connect

# Connect to the Mosquitto MQTT broker (use the service name "mosquitto" as the hostname)
client.connect("mosquitto", 1883, 60)

# Start the client's network loop in a non-blocking manner
client.loop_start()

while True:
    # Randomly select a suffix to simulate three different transmitters
    transmitter_suffix = random.choice(["a", "b", "c"])
    # Publish a message to "/dark-sky/up" with sensor_id test791f921b
    info = client.publish(
        f"dark-sky/up/test791f921{transmitter_suffix}",
        json.dumps(create_test_data_frame(), indent=4),
    )

    # Wait for 1 minute before publishing again
    time.sleep(60)
